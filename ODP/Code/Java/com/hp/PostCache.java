package com.hp;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;
import org.openntf.domino.utils.XSPUtil;

public class PostCache implements Serializable {

	private static final long serialVersionUID = 4616451514594819494L;
	private Map<String, String> entries;
	private Map<String, String> archive;
	private List<String> archiveList;

	public PostCache() {
		init();
		initArchive();
	}

	public void init() {
		this.entries = new HashMap<String, String>();
		ViewNavigator nav = XSPUtil.getCurrentSession().getCurrentDatabase().getView("posts").createViewNav();
		ViewEntry ent = nav.getFirst();
		while (ent != null) {
			String unid = ent.getDocument().getUniversalID();
			String permakey = ent.getDocument().getItemValueString("postKey");
			if (!permakey.trim().equals("")) {
				System.out.println("Caching... " + permakey);
				entries.put(permakey, unid);
			}

			entries.put(ent.getDocument().getItemValueString("noteId"), unid);
			entries.put(ent.getDocument().getNoteID(), unid);

			ent = nav.getNext(ent);
		}
	}

	public void initArchive() {
		this.archive = new HashMap<String, String>();
		this.archiveList = new ArrayList<String>();
		ViewNavigator nav = XSPUtil.getCurrentDatabase().getView("postsarchive").createViewNav();
		ViewEntry ent = nav.getFirst();
		while (ent != null) {
			if (ent.isCategory()) {
				DecimalFormat decimalFormat = new DecimalFormat("#.#");
				archive.put(ent.getColumnValues().elementAt(0).toString(), decimalFormat.format(ent.getColumnValues().elementAt(1)));
				archiveList.add(ent.getColumnValues().elementAt(0).toString() + " (" + decimalFormat.format(ent.getColumnValues().elementAt(1)) + " posts)");
			}
			ent = nav.getNextCategory();
		}
	}

	public synchronized Map<String, String> getEntries() {
		return entries;
	}

	public synchronized Map<String, String> getArchive() {
		return archive;
	}

	public List<String> getArchiveList() {
		return archiveList;
	}

}
