package com.hp;

import java.io.Serializable;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.ViewEntryCollection;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Snippets implements Serializable {

	private static final long serialVersionUID = 7431273608940355006L;

	public ViewEntryCollection getSnippets() {
		try {
			Session session = ExtLibUtil.getCurrentSession();
			Database db = session.getCurrentDatabase();
			Config cfg = new Config();
			Document config = cfg.getConfig();
			Database snippets = session.getDatabase(db.getServer(), config.getItemValueString("pathSnippets"));
			return snippets.getView("snDate").getAllEntries();
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getSubject(Document doc) {
		try {
			if (doc.getItemValueString("Form").equals("snippet")) {
				return doc.getItemValueString("snSubject");
			} else {
				return doc.getItemValueString("poSubject");
			}
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}
}
