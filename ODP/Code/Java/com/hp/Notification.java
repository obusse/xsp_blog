package com.hp;

import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.Document;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.utils.XSPUtil;

public class Notification {
	private String sendto;
	private String from;
	private String subject;
	private String message;
	private String url;

	public Notification() {
		//
	}

	private void init(final String parentId) {
		sendto = "obusse@gmail.com";
		from = "no-reply@notesx.net";
		subject = "A new comment/reply has been posted";
		message = "A new comment/reply has been posted. Klick the link below for details";
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		url = getDocUrl(request, parentId);
	}

	public void sendNewComment(final String parentId) {
		init(parentId);
		subject = "A new comment has been posted";
		message = "A new comment has been posted. Klick for details";
		send();
	}

	public void sendNewReply(final String parentId) {
		init(parentId);
		subject = "A new reply to your comment has been posted";
		message = "A new reply to your comment has been posted. Klick the link below for details";
		send();
	}

	@SuppressWarnings("deprecation")
	private void send() {
		Document mail = XSPUtil.getCurrentSessionAsSigner().getDatabase("", "mail.box").createDocument();
		mail.replaceItemValue("Form", "Memo");
		mail.replaceItemValue("From", from);
		mail.replaceItemValue("Principal", from);
		mail.replaceItemValue("Subject", subject);
		mail.replaceItemValue("SendTo", sendto);
		mail.replaceItemValue("Recipients", sendto);
		mail.replaceItemValue("PostedDate", new Date());
		RichTextItem body = mail.createRichTextItem("body");
		body.appendText(message);
		body.addNewLine(2);
		body.appendText(url);
		mail.save();
	}
	
	@SuppressWarnings("unused")
	private void sendInet() {
		Document mail = XSPUtil.getCurrentDatabase().createDocument();
		mail.replaceItemValue("Form", "Memo");
		mail.replaceItemValue("From", from);
		mail.replaceItemValue("INetFrom", from);
		mail.replaceItemValue("Principal", from);
		mail.replaceItemValue("Subject", subject);
		mail.replaceItemValue("SendTo", sendto);
		mail.replaceItemValue("Recipients", sendto);
		mail.replaceItemValue("PostedDate", new Date());
		RichTextItem body = mail.createRichTextItem("body");
		body.appendText(message);
		body.addNewLine(2);
		body.appendText(url);
		mail.send();
	}

	private String getDocUrl(final HttpServletRequest req, final String parentId) {
		return getURL(req, parentId);
	}

	@SuppressWarnings("deprecation")
	private static String getURL(final HttpServletRequest req, final String parentId) {
		String scheme = req.getScheme(); // http
		String serverName = req.getServerName(); // hostname.com
		int serverPort = req.getServerPort(); // 80
		String contextPath = req.getContextPath(); // /mywebapp
		String servletPath = req.getServletPath(); // /servlet/MyServlet
		String pathInfo = req.getPathInfo(); // /a/b;c=123
		String queryString = req.getQueryString(); // d=789

		// Reconstruct original requesting URL
		StringBuffer url = new StringBuffer();
		url.append(scheme).append("://").append(serverName);

		if ((serverPort != 80) && (serverPort != 443)) {
			url.append(":").append(serverPort);
		}

		url.append(contextPath).append(servletPath);

		if (pathInfo != null) {
			url.append(pathInfo);
		}
		if (queryString != null) {
			url.append("?").append(queryString);
		}

		// get document by unique
		String unid = "";
		String page = "";
		try {
			Document doc = XSPUtil.getCurrentDatabase().getView("unique").getDocumentByKey(parentId, true);
			if (doc == null) {
				page = url.substring(url.lastIndexOf("/"));
				page = page.substring(1, page.lastIndexOf("?"));
			} else {
				unid = doc.getNoteID();
				if (doc.getItemValueString("Form").equals("post"))
					page = "blogpost.xsp" + "?documentId=" + unid;
				if (doc.getItemValueString("Form").equals("tutorial"))
					page = "tutorial.xsp" + "?documentId=" + unid;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (url.toString().indexOf("?") != -1) {
			return url.toString().substring(0, url.toString().lastIndexOf("/")) + "/" + page;
		} else {
			return url.toString() + "/" + page;
		}
	}

	public String getSendto() {
		return sendto;
	}

	public void setSendto(final String sendto) {
		this.sendto = sendto;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(final String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

}
