package com.hp;

import java.io.Serializable;

import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;
import lotus.domino.ViewEntry;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Config implements Serializable {

	private static final long serialVersionUID = 3641649100300664480L;

	public Document getConfig() {
		try {
			// TODO dao bean
			Session session = ExtLibUtil.getCurrentSession();
			View v = session.getCurrentDatabase().getView("all");
			Document doc = v.getDocumentByKey("config");
			return doc;
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getSnippetDb() {
		try {
			Document doc = this.getConfig();
			return doc.getItemValueString("pathSnippets");
		} catch (Exception e) {
			return "";
		}

	}

	public String getBlogDb() {
		try {
			Document doc = this.getConfig();
			return doc.getItemValueString("pathBlog");
		} catch (Exception e) {
			return "";
		}

	}

	public void saveConfig() {
		//
	}

	public String getSnippetpath(ViewEntry ent) {
		try {
			Document doc = this.getConfig();
			String path = doc.getItemValueString("pathSnippets");
			doc.recycle();
			return "/" + path + "/0/" + ent.getDocument().getUniversalID();
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getBlogpath(ViewEntry ent) {
		try {
			Document doc = this.getConfig();
			String path = doc.getItemValueString("pathBlog");
			doc.recycle();
			return "/" + path + "/0/" + ent.getDocument().getUniversalID();
		} catch (NotesException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getMetaKeywords() {
		try {
			Document doc = this.getConfig();
			return doc.getItemValueString("metaKeywords");
		} catch (Exception e) {
			return "";
		}
	}

	public String getMetaDesc() {
		try {
			Document doc = this.getConfig();
			return doc.getItemValueString("metaDesc");
		} catch (Exception e) {
			return "";
		}
	}
}
