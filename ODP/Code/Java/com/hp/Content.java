package com.hp;

import java.io.Serializable;

import lotus.domino.Document;

import org.openntf.domino.utils.XSPUtil;

public class Content implements Serializable {

	private static final long serialVersionUID = -4946595748640985569L;

	@SuppressWarnings("deprecation")
	public String getId(final String key) {
		try {
			Document doc = XSPUtil.getCurrentDatabase().getView("content").getDocumentByKey(key);
			String id = doc.getUniversalID();
			return id;
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("deprecation")
	public String getTitle(final String key) {
		try {
			Document doc = XSPUtil.getCurrentDatabase().getView("content").getDocumentByKey(key);
			String ret = doc.getItemValueString("contentTitle");
			return ret;
		} catch (Exception e) {
			return "[content not found for title]";
		}
	}
}
