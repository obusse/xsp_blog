package com.hp;

import java.io.Serializable;

import lotus.domino.Database;
import lotus.domino.DocumentCollection;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class DocumentCache implements Serializable {

	private static final long serialVersionUID = -6350808167264019176L;

	public DocumentCollection getArchDocs(final String date) {
		DocumentCollection col = null;
		try {
			// date parsing
			String year = date.split("/")[0];
			String month = date.split("/")[1];
			Database db = ExtLibUtil.getCurrentDatabase();
			col = db.createDocumentCollection();
			View v = db.getView("posts");
			ViewNavigator nav = v.createViewNav();
			ViewEntry ent = nav.getFirstDocument();
			String pYear = "";
			String pMonth = "";
			while (ent != null) {
				pYear = (String) ExtLibUtil.getCurrentSession().evaluate("@Text(@Year(postDate))", ent.getDocument()).elementAt(0);
				pMonth = (String) ExtLibUtil.getCurrentSession().evaluate("@Right(\"00\"+@Text(@Month(postDate));2)", ent.getDocument()).elementAt(0);
				if (pYear.equals(year) && pMonth.equals(month)) {
					col.addDocument(ent.getDocument());
				}

				ent = nav.getNextDocument();
			}
			
			nav.recycle();
			v.recycle();
			db.recycle();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return col;
	}
}
