package com.hp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import lotus.domino.Document;

import org.openntf.domino.utils.XSPUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class NavigationFooter implements Serializable {

	private static final long serialVersionUID = -8857086205056457935L;
	private final List<Page> navigation;

	@SuppressWarnings({ "unchecked", "deprecation" })
	public NavigationFooter() {
		boolean admin = false;
		String adminForm = "";
		String adminDoc = "";
		String doctype = "";
		String pageTitle = null;
		Document doc = null;
		this.navigation = new ArrayList<Page>();
		navigation.add(new Page("Imprint", "imprint.xsp", "fa fa-legal", false));
		navigation.add(new Page("Privacy", "privacy.xsp", "fa fa-lock", false));
//		navigation.add(new Page("Intranet", "http://intranet.notesx.net/", "fa fa-shield", true, true));
		try {
			admin = ExtLibUtil.getCurrentDatabase().queryAccessRoles(ExtLibUtil.getCurrentSession().getEffectiveUserName()).contains("[Admin]");
			// render if not admin
			navigation.add(new Page("Login", "login.xsp", "fa fa-unlock", false, !admin));

			// render if admin
			navigation.add(new Page("Logout", ExtLibUtil.getCurrentDatabase().getFilePath() + "?logout&redirectto=" + "/" + ExtLibUtil.getCurrentDatabase().getFilePath(), "fa fa-power-off", false,
					admin));

			// get current document
			FacesContext context = FacesContext.getCurrentInstance();
			try {
				DominoDocument xspdoc = (DominoDocument) ExtLibUtil.resolveVariable(context, "currentDocument");
				doc = xspdoc.getDocument();

				if (doc.getItemValueString("Form").equals("post")) {
					adminForm = "adminblog.xsp";
					adminDoc = "adminPost.xsp";
					doctype = "Post";
					pageTitle = doc.getItemValueString("postTitle");
				} else if (doc.getItemValueString("Form").equals("tutorial")) {
					adminForm = "admintutorials.xsp";
					adminDoc = "adminTutorial.xsp";
					doctype = "Tutorial";
					pageTitle = doc.getItemValueString("tutTitle");
				} else {
					adminForm = "admin.xsp";
					adminDoc = "admincontent.xsp";
					doctype = "Page";
					pageTitle = doc.getItemValueString("contentTitle");
				}

			} catch (Exception e) {
				adminForm = "admin.xsp";
				adminDoc = "admincontent.xsp";
				doctype = "Page";

			}

			if (doc != null) {
				// page shows a post or tutorial
				navigation.add(new Page("Edit " + doctype, adminDoc + "?action=editDocument&documentId=" + doc.getUniversalID(), "fa fa-edit", false, admin));
			} else {
				// page show a content page
				HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
				String contentKey = req.getRequestURI().toString();
				contentKey = contentKey.substring(contentKey.lastIndexOf("/") + 1);

				Document contentDoc = XSPUtil.getCurrentDatabase().getView("content").getDocumentByKey(contentKey);
				if (contentDoc != null) {
					pageTitle = contentDoc.getItemValueString("contentTitle");
					navigation.add(new Page("Edit " + doctype, adminDoc + "?action=editDocument&documentId=" + contentDoc.getUniversalID(), "fa fa-edit", false, admin));
				} else {
					contentDoc = XSPUtil.getCurrentDatabase().getView("content").getDocumentByKey(contentKey.replace(".xsp", ""));
					if (contentDoc != null) {
						pageTitle = contentDoc.getItemValueString("contentTitle");
						navigation.add(new Page("Edit " + doctype, adminDoc + "?action=editDocument&documentId=" + contentDoc.getUniversalID(), "fa fa-edit", false, admin));
					}
				}
			}
			navigation.add(new Page("Admin", adminForm, "fa fa-cog fa-spin", false, admin));

			// set page title
			UIViewRoot viewRoot = context.getViewRoot();
			pageTitle = (pageTitle != null) ? pageTitle + " // Oliver Busse" : "Oliver Busse";
			viewRoot.getAttributes().put("pageTitle", pageTitle);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Page> getNavigation() {
		return navigation;
	}
}
