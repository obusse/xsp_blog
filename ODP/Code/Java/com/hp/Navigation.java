package com.hp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Navigation implements Serializable {

	private static final long serialVersionUID = 8031965383531253276L;
	private final List<Page> navigation;

	public Navigation() {
		this.navigation = new ArrayList<Page>();
		// navigation.add(new Page("About", "about.xsp",
		// "glyphicon glyphicon-user", false));
//		navigation.add(new Page("My Music", "https://soundcloud.com/zeromancer1972", "glyphicon glyphicon-music", true));
		navigation.add(new Page("Blog", "blog.xsp", "glyphicon glyphicon-tasks", false));
		navigation.add(new Page("Tutorials", "tutorials.xsp", "glyphicon glyphicon-hand-up", false));
		navigation.add(new Page("Docs", "docu.xsp", "glyphicon glyphicon-facetime-video", false));
//		navigation.add(new Page("My Todo List", "todos.xsp", "glyphicon glyphicon-ok", false));
//		navigation.add(new Page("Bookmarks", "/obpractise.nsf/index3.xsp", "glyphicon glyphicon-bookmark", true));
//		navigation.add(new Page("Downloads", "/Privat/filesilo.nsf/index.xsp?key=hpdownloads", "glyphicon glyphicon-cloud-download", true));
		navigation.add(new Page("Slide Decks", "http://de.slideshare.net/OliverBusse", "fa fa-slideshare", true));
//		navigation.add(new Page("GitHub", "github.xsp", "fa fa-github", false));
//		navigation.add(new Page("Imprint", "imprint.xsp", "fa fa-legal", false));
		navigation.add(new Page("Terms", "terms.xsp", "fa fa-bookmark", false));
//		navigation.add(new Page("Privacy", "privacy.xsp", "fa fa-lock", false));
//		navigation.add(new Page("My Repository", "http://notesx.net:8090/", "fa fa-github-alt", true));
		// navigation.add(new Page("Subscribe", "rss.xsp", "fa fa-rss", true));
		navigation.add(new Page("Domino Navigator", "domnav.xsp", "fa fa-arrow-right", false));
		navigation.add(new Page("More", "other.xsp", "fa fa-plus", false));

	}

	public List<Page> getNavigation() {
		return navigation;
	}
}
