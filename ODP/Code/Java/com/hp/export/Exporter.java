package com.hp.export;

import java.io.Serializable;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NoteCollection;
import lotus.domino.Session;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Exporter implements Serializable {

	private static final long serialVersionUID = 1L;

	public void exportAllBlogPosts() {
		try {
			BlogPostExporter exporter = new BlogPostExporter();
			Session session = ExtLibUtil.getCurrentSession();
			Database db = session.getCurrentDatabase();
			NoteCollection col = db.createNoteCollection(false);
			col.setSelectDocuments(true);
			col.setSelectionFormula("form=\"post\"");
			col.buildCollection();
			String noteId = col.getFirstNoteID();
			while (noteId.length() > 0) {
				Document doc = db.getDocumentByID(noteId);
				exporter.doExport(doc);
				noteId = col.getNextNoteID(noteId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
