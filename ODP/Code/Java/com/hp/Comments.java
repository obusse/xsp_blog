package com.hp;

import lotus.domino.Document;
import lotus.domino.DocumentCollection;
import lotus.domino.NotesException;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Comments {
	public DocumentCollection getComments(final String key)
			throws NotesException {
		return ExtLibUtil.getCurrentDatabase().getView("comments")
				.getAllDocumentsByKey(key, true);
	}

	// send email to every one in the comment thread if new comment is posted
	public void notifyComment(final Document doc) {
		System.out
				.println("comment approved, sending mails to all commentators");

	}

}
