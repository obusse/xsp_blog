package com.hp.graph;

import org.openntf.domino.graph2.annotations.AdjacencyUnique;
import org.openntf.domino.graph2.builtin.DVertexFrame;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.frames.Property;
import com.tinkerpop.frames.modules.typedgraph.TypeValue;

@TypeValue("Blogpost")
public interface Blogpost extends DVertexFrame {

	@Property("postTitle")
	public String getTitle();
	
	@Property("noteId")
	public String getId();

	@AdjacencyUnique(label = "relates", direction = Direction.IN)
	public Iterable<Blogpost> getRelatedPosts();

	@AdjacencyUnique(label = "relates", direction = Direction.IN)
	public Blogpost addRelated(Blogpost post);

	@AdjacencyUnique(label = "relates", direction = Direction.IN)
	public void removeRelated(Blogpost post);

}
