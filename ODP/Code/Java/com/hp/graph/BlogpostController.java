package com.hp.graph;

import java.util.ArrayList;
import java.util.List;

import org.openntf.domino.graph2.impl.DConfiguration;
import org.openntf.domino.graph2.impl.DElementStore;
import org.openntf.domino.graph2.impl.DFramedGraphFactory;
import org.openntf.domino.graph2.impl.DFramedTransactionalGraph;
import org.openntf.domino.graph2.impl.DGraph;
import org.openntf.domino.xsp.XspOpenLogUtil;

import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class BlogpostController {

	public static List<BlogpostObject> getRelatedPosts(DominoDocument doc) {
		List<BlogpostObject> list = new ArrayList<BlogpostObject>();
		DFramedTransactionalGraph<DGraph> framedGraph = setupGraph();
		try {
			Blogpost blogpost = framedGraph.addVertex(doc.getItemValueString("noteId"), Blogpost.class);
			for (Blogpost post : blogpost.getRelatedPosts()) {
				BlogpostObject postObject = new BlogpostObject();
				postObject.setTitle(post.getTitle());
				postObject.setId(post.getId());
				list.add(postObject);
			}
		} catch (Exception e) {
			XspOpenLogUtil.logError(e);
		}
		return list;
	}

	private static DFramedTransactionalGraph<DGraph> setupGraph() {
		try {
			DElementStore sessionStore = new DElementStore();
			sessionStore.addType(Blogpost.class);
			sessionStore.setStoreKey(ExtLibUtil.getCurrentDatabase().getFilePath());
			DConfiguration config = new DConfiguration();
			DGraph graph = new DGraph(config);
			DFramedGraphFactory factory = new DFramedGraphFactory(config);
			DFramedTransactionalGraph<DGraph> fg = (DFramedTransactionalGraph<DGraph>) factory.create(graph);
			return fg;
		} catch (Throwable t) {
			XspOpenLogUtil.logError(t);
			return null;
		}
	}
}
