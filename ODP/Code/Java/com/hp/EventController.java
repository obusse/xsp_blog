package com.hp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Locale;

import lotus.domino.DateTime;
import lotus.domino.NotesException;

import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class EventController implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getEventDate(DominoDocument doc) {
		String date = "/tba";
		Locale locale = Locale.ENGLISH;
		SimpleDateFormat f = new SimpleDateFormat("MMMM dd, yyyy", locale);
		SimpleDateFormat sf = new SimpleDateFormat("MMMM dd", locale);
		SimpleDateFormat day = new SimpleDateFormat("dd", locale);
		SimpleDateFormat year = new SimpleDateFormat("yyyy", locale);
		double oneDay = 86400;
		try {
			if (doc.hasItem("eventDate") && doc.hasItem("eventDateTo")) {
				DateTime from = doc.getItemValueDateTime("eventDate");
				DateTime to = doc.getItemValueDateTime("eventDateTo");
				
				date = sf.format(from.toJavaDate()) + " - " + day.format(to.toJavaDate()) + ", "
				+ year.format(from.toJavaDate());

				// one day
				if (from.toJavaDate().getTime() == to.toJavaDate().getTime()) {
					date = f.format(from.toJavaDate());
				}

				// two days
				if (to.timeDifference(from) == oneDay) {
					date = sf.format(from.toJavaDate()) + " + " + day.format(to.toJavaDate()) + ", "
							+ year.format(from.toJavaDate());
				}
			}
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

}
