package org.openntf.xsp.dojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lotus.domino.NotesException;

import com.ibm.xsp.context.DojoLibrary;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Config implements Serializable {

	private static final long serialVersionUID = -5429830701050906773L;
	private final Map<String, String> config;
	private final boolean fp2;
	private boolean tenOrAbove = false;

	public Config() {
		this.config = new HashMap<String, String>();
		DojoLibrary dojolib = com.ibm.xsp.context.DojoLibraryFactory.getDefaultLibrary();
		// System.out.println(dojolib.getVersion().toString());
		this.fp2 = dojolib.getVersion().toString().indexOf("1.9") != -1;
		
		try {
			String version = ExtLibUtil.getCurrentSession().evaluate("@Version").elementAt(0).toString();
			int versionNumber = Integer.valueOf(version);
			this.tenOrAbove = versionNumber >= 450;
		} catch (NotesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// get the current version of Dojo
		config.put("version", dojolib.getVersion().toString());
		// get the keyword for setting toolbar options
		config.put("toolbartype", this.isFp2() ? "toolbar" : "toolbarType");
		
		// to be extended...
	}

	public Map<String, String> getConfig() {
		return config;
	}

	public boolean isFp2() {
		return fp2;
	}

	public boolean isTenOrAbove() {
		return tenOrAbove;
	}
	
}
