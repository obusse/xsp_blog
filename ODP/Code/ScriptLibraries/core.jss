function getRSS(){
	var exCon = facesContext.getExternalContext(); 
	var writer = facesContext.getResponseWriter();
	var response = exCon.getResponse();
	
	var v:NotesView
	v = database.getView('postsrss')
	var doc:NotesDocument
	
	var dbURL = facesContext.getExternalContext().getRequest().getRequestURL()
	dbURL = @LeftBack(dbURL, '/')

	response.setContentType("text/xml; charset=UTF-8");
	response.setHeader("Cache-Control", "no-cache");
	
	writer.write('<?xml version="1.0" encoding="utf-8"?>');
	writer.write('<rss version="2.0">')
	writer.write('<channel>')
	writer.write('<title>'+database.getTitle()+'</title>')
	writer.write('<link>'+dbURL+'</link>')
	writer.write('<description>'+database.getTitle()+'</description>')
	writer.write('<language>en-us</language>')
	writer.write('<copyright>Oliver Busse</copyright>')
	
	
	doc = v.getFirstDocument();
	
	var count = 0;
	
	var title = ""
	var content = ""
	var date = ""
	var url = ""
		
	var addPubDate = true;
	
	while(doc!=null && count < 5){
		
		if(doc.getItemValueString("Form").equals("post")){
			title = doc.getItemValueString("postTitle");
			title = title.replaceAll("&", "&amp;").replaceAll("<", "&#60;").replaceAll(">", "&#62;");
			content = doc.getMIMEEntity('postBody').getContentAsText()
			dt = doc.getItemValueDateTimeArray("postDate").elementAt(0);
			date = new java.text.SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", java.util.Locale.US).format(dt.toJavaDate());
			url = dbURL+'/blogpost.xsp?documentId='+doc.getNoteID();
			count++;
		}
		
		if(addPubDate){
			writer.write('<pubDate>'+date+'</pubDate>');
			addPubDate = false;
		}
		
		writer.write('<item>')
		writer.write('<title>'+title+'</title>')
		/*
		try {
			writer.write('<description><![CDATA['+content+']]></description>')
		} catch(e) {
			
		}
		*/
		writer.write('<link>'+url+'</link>')
		writer.write('<guid>'+url+'</guid>');
		//writer.write('<author>'+@Name('[CN]', doc.getItemValueString('snAuthor'))+'</author>')
		writer.write('<author>kontakt@oliverbusse.com (Oliver Busse)</author>')
		//writer.write('<guid isPermaLink="false">'+doc.getNoteID()+'</guid>')
		writer.write('<pubDate>'+date+'</pubDate>')
		writer.write('</item>')
		doc = v.getNextDocument(doc)
	}
	
	writer.write('</channel>')
	writer.write('</rss>')
	writer.endDocument();
	facesContext.responseComplete();
}