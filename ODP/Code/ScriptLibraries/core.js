// core JS

$(document).ready( function() {
	/*
	 * if (location.href.indexOf("index.xsp") != -1) { $.bootstrapGrowl("Please
	 * update your bookmarks to the new URL!", { ele : "#content", type :
	 * "warning", align : "right", offset : { from : "bottom", amount : 65 },
	 * 
	 * delay : 10000 }); }
	 */

	try {
		prettyPrint();
	} catch (e) {
	}
});

function dismissBulletin() {
	createCookie("obusse_bulletin", "1", 999);
}

// projects
function prj(id) {
	dojo.byId("prjdata").src = "";
	dojo.byId("prjdata").src = "projectdetails.xsp?id=" + id;
	var dialog = dijit.byId("prj");
	dialog.set("autofocus", false);
	dialog.show();
}

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else
		var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}